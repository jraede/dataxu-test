angular.module('app', ['restangular', 'angular-growl'])
.config(['RestangularProvider', 'growlProvider', function(RestangularProvider, growlProvider) {
	RestangularProvider.setRestangularFields({
		id:'_id'
	});

	growlProvider.globalTimeToLive(3000);
}])
.run(['Restangular', function(Restangular) {
	Restangular.setFullResponse(true);
}]);
