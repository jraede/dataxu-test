angular.module('app').controller('UserCtrl', ['$scope', 'Restangular', '$timeout', '$window', 'growl', '$filter', function($scope, Restangular, $timeout, $window, growl, $filter) {

	var confirmMessage = 'You have unsaved changes on this user. Are you sure you want to continue?';
	

	$scope.pagination = {
		perPage:10,
		page:1
	};

	$window.onbeforeunload = function() {
		if($scope.editing && $scope.editing.$$dirty) {
			return confirmMessage;
		}
		return null;
	};

	$scope.editing = false;
	$scope.tableSort = {
		sorter:'name.last'
	};

	

	updatePagination = function() {

		// Users will be paginated based on filter. Use it here.
		users = $filter('filter')($scope.users, $scope.query);
		$scope.pagination.totalUsers = users.length;
		$scope.pagination.total = Math.ceil(users.length/$scope.pagination.perPage);

		
		var end = $scope.pagination.page * $scope.pagination.perPage;
		if(end > users.length) {
			$scope.pagination.end = users.length;
		}
		else {
			$scope.pagination.end = end;
		}

		if(!users.length) {
			$scope.pagination.start = 0;
		}
		else {
			$scope.pagination.start = ($scope.pagination.page - 1) * $scope.pagination.perPage + 1;
		}

		
	};


	// Stops watching, etc. If they were editing an unsaved user, delete that badboy
	stopEditing = function() {
		if(!$scope.editing._id) {
			var index = $scope.users.indexOf($scope.editing);
			if(index >= 0) {
				$scope.users.splice(index, 1);
			}
		}
		$scope.editing = null;
		
		if($scope.unWatch && _.isFunction($scope.unWatch)) {
			$scope.unWatch();
		}

		updatePagination();
	};


	// If they are currently editing one and have modified it, make them confirm
	checkForEditing = function() {
		if(!$scope.editing) {
			return true;
		}

		if($scope.editing.$$dirty) {
			if($window.confirm(confirmMessage)) {
				stopEditing();
				return true;
			}
			return false;
		}

		else {
			return true;
		}
	};

	
	


	// Load users from the DB.

	Restangular.all('users').getList().then(function(response) {
		$scope.users = response.data;
		
		updatePagination();

		$scope.$watch('pagination.page', updatePagination);
		$scope.$watch('query', updatePagination);
		// Bootstrap everything here since we know we have the users
		

		/**
		 * Add a new user and edit. If currently editing, confirm first.
		 */
		$scope.addUser = function() {
			if(checkForEditing()) {
				// If they're on a different page it won't show up. Jump to first page.
				$scope.pagination.page = 1;
				var user = Restangular.one('users');
				user.active = false;

				$scope.users.push(user);
				$scope.editUser(user);
			}

		};

		/**
		 * Set a user's `form` property back to original values
		 */
		$scope.revertUser = function(user) {

			// Could get hacky with larger objects, but this is OK for now since it's small
			if(!user.name) {
				user.name = {
					first:'',
					last:''
				};
			}


			user.form = {
				name:{
					first:user.name.first,
					last:user.name.last
				},
				email:user.email,
				age:user.age,
				active:user.active
			};
		};


		/**
		 * Edit a user. The inline form controls the `form` property so we can revert
		 */
		$scope.editUser = function(user) {
			if($scope.editing === user) {
				return;
			}

			if(checkForEditing()) {
				$scope.revertUser(user);

				$scope.editing = user;
				$scope.editing.$$dirty = false;


				// Watch for changes on the form and mark it as dirty if needed. Used in checkForEditing()
				$scope.unWatch = $scope.$watch('editing.form', function() {
					$scope.editing.$$dirty = true;
				});
			}

			
			
		};

		/**
		 * Saves a user. Displays success/error messaging
		 */
		$scope.saveUser = function(user) {
			if($scope.userForm.$valid) {
				// Form is valid, let's save
				_.extend(user, user.form);
				
				var method;
				if(user._id) {
					method = 'put';
				}
				else {
					method = 'post';
				}

				user[method]().then(function(response) {
					growl.addSuccessMessage('Saved user!');

					// Update id and createdOn and editedOn from server so we don't have to fetch everything
					user.editedOn = response.data.editedOn;
					user.createdOn = response.data.createdOn;
					user._id = response.data._id;
					stopEditing();

				}, function(err) {
					growl.addErrorMessage('Failed to save user!');
				});
			}
		};

		/**
		 * Removes a user
		 */
		$scope.deleteUser = function(user) {
			if($window.confirm('Are you sure you want to delete this user?')) {
				
				if(user._id) {
					user.remove().then(function() {
						var index = $scope.users.indexOf(user);
						$scope.users.splice(index, 1);

						growl.addSuccessMessage('User deleted successfully');
						updatePagination();
					}, function(err) {
						growl.addErrorMessage('Failed to delete user.');
					});
				}
				else {
					var index = $scope.users.indexOf(user);
					$scope.users.splice(index, 1);
					growl.addSuccessMessage('User deleted successfully');
					updatePagination();
				}
				if(user === $scope.editing) {
					stopEditing();
				}
				
			}
		};

		/**
		 * Go to next page of results
		 */
		$scope.nextPage = function() {
			if($scope.pagination.total > $scope.pagination.page) {
				$scope.pagination.page++;
			}
		}

		/**
		 * Go to prev page of results
		 */
		$scope.prevPage = function() {
			if($scope.pagination.page > 1) {
				$scope.pagination.page--;
			}
		};

		/**
		 * Toggle a user's active field (in the form)
		 */
		$scope.toggleActive = function(user) {
			user.form.active = !user.form.active;
		};




	}, function(err) {
		growl.addErrorMessage('Failed to load users!');
	});



}]);