angular.module('app').filter('slice', function() {
	return function(arr, start, end) {
		if(arr && arr instanceof Array) {
			return arr.slice(start, end);
		}
		return arr;
		
	};
});