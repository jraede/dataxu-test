angular.module('app').directive('tableSort', ['$parse', function($parse) {

	return {
		restrict:'A',
		scope: {
			tableSort:'='
		},
		controller: ['$scope', '$compile', '$http', function($scope, $compile, $http) {

			$scope.changeSort = function(field) {
				if($scope.tableSort.sorter === field) {
					$scope.tableSort.sorter = '-' + field;
				}
					
				else {
					$scope.tableSort.sorter = field;
				}
					
			};
			return $scope;
		}]
	};

}]).directive('sortColumn', function() {
	return {
		restrict:'A',
		require: '^tableSort',
		scope:true,
		template:'<a disable-selection ng-click="changeSort(sortColumn)"><span ng-transclude></span><span class="pull-right"><i class="fa fa-caret-up" ng-show="sortingAsc()"></i><i class="fa fa-caret-down" ng-show="sortingDesc()"></i></span></a>',
		transclude: true,

		link: function($scope, element, attrs, sortCtrl) {
			$scope.sortColumn = attrs.sortColumn;

			$scope.sortingAsc = function() {
				if(sortCtrl.tableSort.sorter === $scope.sortColumn) {
					return true;
				}
					
				return false;
			};

			$scope.sortingDesc = function() {
				if(sortCtrl.tableSort.sorter === '-' + $scope.sortColumn) {
					return true;
				}
				return false;
			};
				
			$scope.changeSort = function() {
				sortCtrl.changeSort.apply(sortCtrl, arguments);
			};
		}
				
	};
});
	