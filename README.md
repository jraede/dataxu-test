# DataXu Test

This is a demo application for interacting with a list of users.

## Installation
You'll need lineman:

`npm install -g lineman`.

Then install the dependencies in the repository root:

`npm install`

You will need to create a file called `.env` in the root with one entry:

`MONGOHQ_URL=mongodb://path/to/mongodb`

And of course have Mongo installed.

## Development
`lineman run`

## Front-end tests
`lineman spec`

## Front-end tests in CI (phantomJS)
`lineman spec-ci`

## Server API tests
`lineman grunt test-server`

## Known Issues
* Changing sort when adding a new user could hide that user.
* NG-cloak probably needed to prevent initial flicker

