/**
 * Boots the server by connecting to all databases and registering models with mongoose
 */

var Q = require('q');

// Promise for booting the server, in case something fails and you want to log
deferred = Q.defer()

require('./lib/services/modelIndex').register().then(function() {
	console.log('Registered models');

	try {
		// For now just mongo, but add redis here if needed
		Q.all([require('./lib/mongo').connect()]).then(function() {
			console.log('Connected to databases. Starting app.');

			try {
				var app = require('./lib/server').boot();
				deferred.resolve(app);

			}
			catch(err) {
				deferred.reject(err);
			}
		}, function(err) {
			deferred.reject(err);
		});
	}
	catch(err) {
		deferred.reject(err);
	}
});

module.exports = deferred.promise



