var request = require('supertest');
var should = require('should');

var boot = require('../boot');
var mongoose = require('mongoose');
var moment = require('moment');
var _ = require('underscore');


describe('Users endpoint', function() {
	this.timeout(5000);
	before(function(done) {

		var self = this;
		boot.then(function(app) {
			var Session = require('supertest-session')({
				app:app
			});

			self.sess = new Session();
			done();
		});
	});


	describe('Create a user', function() {
		afterEach(function(done) {
			require('../lib/mongo').drop().then(function() {
				done();
			});
		});

		it('should let you post a user', function(done) {
			var user = {
				name:{
					first:'Testy',
					last:'McGee'
				},
				age:20,
				email:'test@test.com',
				active:true,

			};

			this.sess.post('/users').send(user).end(function(err, res) {
				res.status.should.equal(201);
				res.body.name.first.should.equal('Testy');
				res.body.name.last.should.equal('McGee');
				moment(res.body.createdOn).format('YYYY-MM-DD').should.equal(moment().format('YYYY-MM-DD'));
				moment(res.body.editedOn).format('YYYY-MM-DD').should.equal(moment().format('YYYY-MM-DD'));
				done();
			});

		});

		it('should throw a 400 if you are missing something required', function(done) {
			var user = {
				name:{
					first:'Testy'
				},
				age:20,
				email:'test@test.com',
				active:true,

			};

			this.sess.post('/users').send(user).end(function(err, res) {
				res.status.should.equal(400);
				done();
			});
		});
	});

	describe('Edit a user', function() {
		beforeEach(function(done) {
			var user = {
				name:{
					first:'Testy',
					last:'McGee'
				},
				age:20,
				email:'test@test.com',
				active:true,

			};

			var self = this;
			this.sess.post('/users').send(user).end(function(err, res) {
				self.user = res.body;
				done();
			});
		});
		afterEach(function(done) {
			require('../lib/mongo').drop().then(function() {
				done();
			});
		});

		it('should let you change user info', function(done) {
			this.user.active = false;

			this.sess.put('/users/' + this.user._id).send(this.user).end(function(err, res) {
				res.status.should.equal(200);
				res.body.active.should.equal(false);
				done();
			});

		});
		it('should update editedOn', function(done) {
			var originalEditedOn = moment(this.user.editedOn);
			this.sess.put('/users/' + this.user._id).send(this.user).end(function(err, res) {
				res.status.should.equal(200);
				originalEditedOn.isBefore(moment(res.body.editedOn)).should.equal(true);
				done();
			});
		});
	});
});