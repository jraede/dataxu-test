url = require('url')
config = {}

if process.env.REDISTOGO_URL?
    redisUrl = url.parse(process.env.REDISTOGO_URL)

else 
	redisUrl = url.parse('redis://:@127.0.0.1:6379/0')

config.protocol = redisUrl.protocol.substr(0, redisUrl.protocol.length - 1)
config.username = redisUrl.auth.split(':')[0]
config.password = redisUrl.auth.split(':')[1]
config.host = redisUrl.hostname
config.port = redisUrl.port
config.database = redisUrl.path.substring(1)

config.secret = "2348sflse2348wdfsli23409weusldfhsl1028sifsdlfi23408sdfli"

module.exports = config