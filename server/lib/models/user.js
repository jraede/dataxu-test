var mongoose = require('mongoose');

var User = new mongoose.Schema({
	name:{
		last:{
			type:String,
			required:true
		},
		first:{
			type:String,
			required:true
		}
	},
	age:Number,
	email:{
		type:String,
		required:true
	},
	createdOn:{
		type:Date,
		default:Date.now
	},
	editedOn:Date,
	active:{
		type:Boolean,
		default:false
	}

});


mongoose.model('User', User);