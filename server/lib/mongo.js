var Q = require('q');
var mongoose = require('mongoose');
module.exports = {
	connection:null,

	/**
	 * Connects to mongo based on environment variables
	 * 
	 * @return promise
	 */
	connect:function() {
		var deferred = Q.defer();
		if(process.env.NODE_ENV === 'test') {
			mongoose.connect('mongodb://localhost/dataxu_test');
		}
		else {
			mongoose.connect(process.env.MONGOHQ_URL);
		}

		if(process.env.NODE_ENV !== 'production') {
			mongoose.set('debug', true);
		}

		this.connection = mongoose.connection;

		mongoose.connection.on('error', function(err) {
			console.error(err);
			// Reject the deferred; if it's the first time it'll actually respond, if not
			// then nothing will happen. But no need to duplicate functionality for initial
			// error handling
			deferred.reject(err);
		});

		mongoose.connection.on('connected', function() {
			console.log('Mongo connected - resolving promise');
			deferred.resolve();
		});

		return deferred.promise;
	},

	$$dropCollection:function(col) {
		var deferred = Q.defer();
		if(process.env.NODE_ENV !=='test') {
			deferred.reject('Cannot drop production collection');

		}
		else {
			col = col.name.split('.');
			if(col[1] === 'system') {
				deferred.resolve();
			}
			else {
				var collectionName = col.pop();
				mongoose.connection.db.dropCollection(collectionName, function(err) {
					if(err) {
						return deferred.reject(err);
					}
					deferred.resolve();
				});
			}
		}
		return deferred.promise;
	},
	drop:function() {
		var deferred = Q.defer();
		if(process.env.NODE_ENV !== 'test') {
			deferred.reject('Cannot drop production db');
		}
		else {
			var self = this;
			mongoose.connection.db.collectionNames(function(err, collections) {
				if(err) {
					return deferred.reject(err);
				}

				var promises = [];
				var col;
				for(var i=0;i<collections.length;i++) {
					col = collections[i];
					promises.push(self.$$dropCollection(col));
				}
				Q.all(promises).then(function() {
					deferred.resolve();
				}, function(err) {
					deferred.reject(err);
				});
			});
		}
		return deferred.promise;
	}
		
			
			



}