var _ = require('underscore');

module.exports = {
	boot:function() {
		console.log('Booting server');

		var express = require('express');
		var app = express();

		if(app.get('env') === 'development' || app.get('env') === 'test') {
			app.use(express.errorHandler());
			app.use(express.logger('dev'));
		}
		else {
			console.log = function() {
				return;
			}
		}


		var PORT = process.env.PORT;
		if(!PORT || typeof(PORT) === undefined) {
			PORT = 8000;
		}

		// Test uses port 8001 so we can run it concurrently with dev server
		if(app.get('env') === 'test') {
			PORT = 8001;
		}


		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(express.cookieParser());

		var localPath = require('fs').realpathSync('');
		var publicPath;
		if(app.get('env') === 'development') {
			publicPath = localPath + '/generated/';
		}
		else {
			publicPath = localPath + '/dist/';
		}

		app.use(express.static(publicPath));


		// Load endpoints
		try {
			var MRE = require('mongoose-rest-endpoints').endpoint;
			new MRE('/users', 'User').tap('pre_save', 'put', function(req, document, next) {
				document.editedOn = new Date();
				return next(document);
			}).register(app);
		}
		catch(err) {
			console.log(err.stack);
		}
		


		app.listen(PORT, function() {
			console.log('Express server listing on port ' + PORT);
		});



		return app;
	}
};