/**
 * Handles loading and retrieval of models and schemas.
 */

var _ = require('underscore');
var Q = require('q');

module.exports = {
	/**
	 * Technically this is synchronous but use a promise here in case we ever have latency or want to change
	 * the fs methods to async
	 * 
	 * @return promise
	 */
	register:function() {
		var deferred = Q.defer();

		var fs = require('fs');

		// Load all models from the models dir
		var list = fs.readdirSync(fs.realpathSync('./server/lib/models'));

		try {
			var file;
			for(var i=0;i<list.length;i++) {
				file = list[i];
				require('../models/' + file.replace('.js', ''));
				console.log('Loaded ', file);
			}
			deferred.resolve();
		}
		catch(err) {
			console.log(err.stack);
			deferred.reject(err);
		}

		return deferred.promise;
	}

}