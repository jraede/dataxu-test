module.exports = (lineman) ->
	config:
		loadNpmTasks: lineman.config.application.loadNpmTasks.concat("grunt-angular-templates", 'grunt-exec')

		removeTasks:

			common: lineman.config.application.removeTasks.common.concat("handlebars", "jst", "jshint")
			dev:lineman.config.application.removeTasks.common.concat('server')

		prependTasks:
			common: lineman.config.application.prependTasks.common.concat("ngtemplates")
			dev:lineman.config.application.prependTasks.common.concat('foreman')


		ngtemplates:
			app:
				options:
					base: "app/templates"
					url: (url) ->
						return url.replace('app/templates/', '')
			
				src: "app/templates/**/*.html"
				dest: "<%= files.ngtemplates.dest %>"

		watch:
			ngtemplates:
				files: "app/templates/**/*.html",
				tasks: ["ngtemplates", "coffee", "concat_sourcemap:admin"]

		files:
			ngtemplates:
				dest: "generated/angular/template-cache.js"
