
module.exports = require(process.env['LINEMAN_MAIN']).config.extend 'files', 
	coffee: 
		app:[

			'app/js/app.coffee', 
			'app/js/services/**/*.coffee',
			'app/js/directives/**/*.coffee',
			'app/js/filters/**/*.coffee',
			'app/js/controllers/app/**/*.coffee'
		]

	js: 
		vendor: [

			"vendor/js/underscore.js",
			"vendor/js/angular.js",
			"vendor/js/restangular.js",
			"vendor/js/angular-growl.js"

		]
	
		
		minified: "dist/js/app.js"
		minifiedWebRelative: "js/app.js"


	templates:
    	source: "app/templates/**/*.html"

	
