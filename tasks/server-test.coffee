fs = require 'fs'
f = require('util').format
module.exports = (grunt) ->
	

	grunt.config.set 'exec.test', 
		cmd:(ex) ->
			return f('NODE_ENV=test mocha --compilers coffee:coffee-script %s', ex)
		
	grunt.registerTask 'dropTestDb', ->
		mongoose = require 'mongoose'
		done = @async()
		mongoose.connect('mongodb://localhost/dataxu_test')
		mongoose.connection.on 'open', ->
			mongoose.connection.db.dropDatabase (err) ->
				if err
					return grunt.log.error('Error dropping database')
				else
					grunt.log.writeln('Dropped test DB')
				mongoose.connection.close(done)


	grunt.registerTask 'test-server', (test='') ->
		

		if test
			tasks = ['dropTestDb', 'exec:test:"server/test/' + test + '.coffee"', 'dropTestDb']
		else
			tasks = ['dropTestDb']
			files = fs.readdirSync('server/test')
			for file in files
				tasks.push 'exec:test:"server/test/' + file + '"'
				tasks.push 'dropTestDb'
		grunt.task.run(tasks)