describe("controller: UserCtrl", function() {
	beforeEach(function() {
		module('app');
	});

	beforeEach(inject(function($controller, $timeout, $httpBackend, $rootScope, Restangular, $window, growl) {
		this.$timeout = $timeout;
		this.$httpBackend = $httpBackend;
		this.growl = growl;
		this.$scope = $rootScope.$new();
		this.$httpBackend.expectGET('/users').respond([]);
		this.$window = $window;

		$controller('UserCtrl', {
			$scope:this.$scope,
			Restangular:Restangular,
			$timeout:$timeout,
			$window:$window,
			growl:growl
		});
		this.$timeout.flush();
		this.$httpBackend.flush();
	}));

	afterEach(function() {
		this.$httpBackend.verifyNoOutstandingExpectation();
	});

	it('should bootstrap with users as array', function() {
		expect(this.$scope.users instanceof Array).toEqual(true);
		
	});

	it('should let you add a user and set that to editing', function() {
		this.$scope.addUser();

		expect(this.$scope.users.length).toEqual(1);

		expect(this.$scope.editing).toEqual(this.$scope.users[0]);
	});

	it('should let you revert a user back to original', function() {
		this.$scope.addUser();
		this.$scope.editing.form.name.first = 'Testy';

		expect(this.$scope.users[0].form.name.first).toEqual('Testy');

		// Now revert
		this.$scope.revertUser(this.$scope.editing);

		expect(this.$scope.users[0].form.name.first).toEqual('');
	});

	// This will fail by way of the afterEach
	it('should not let you save a user if form is invalid', function() {
		this.$scope.addUser();

		this.$scope.userForm = {
			$invalid:true,
			$valid:false
		};

		this.$scope.saveUser(this.$scope.editing);

	});

	it('should show the success message on successfully saving', function() {
		this.$scope.addUser();
		this.$scope.userForm = {
			$valid:true,
			$invalid:false
		};

		this.$scope.editing.form = {
			name:{
				first:'Testy',
				last:'McGee'
			},
			email:'test@test.com'
		}

		this.$httpBackend.expectPOST('/users').respond({
			createdOn:new Date(),
			editedOn:new Date()
		});

		this.$scope.saveUser(this.$scope.editing);
		spyOn(this.growl, 'addSuccessMessage');
		this.$httpBackend.flush();
		expect(this.growl.addSuccessMessage).toHaveBeenCalled();
		expect(this.$scope.editing).toEqual(null);

	});

	it('should show error message on failure to save', function() {
		this.$scope.addUser();
		this.$scope.userForm = {
			$valid:true,
			$invalid:false
		};

		this.$scope.editing.form = {
			name:{
				first:'Testy',
				last:'McGee'
			},
			email:'test@test.com'
		}

		this.$httpBackend.expectPOST('/users').respond(400);

		this.$scope.saveUser(this.$scope.editing);
		spyOn(this.growl, 'addErrorMessage');
		this.$httpBackend.flush();
		expect(this.growl.addErrorMessage).toHaveBeenCalled();
	});

	it('should confirm if you want to delete a user', function() {
		this.$scope.addUser();
		spyOn(this.$window, 'confirm');
		this.$scope.deleteUser(this.$scope.editing);
		expect(this.$window.confirm).toHaveBeenCalled();


	});

	it('should delete a user', function() {

		this.$window.confirm = function() {
			return true;
		};

		this.$scope.addUser();



		// Give it a bogus ID
		this.$scope.editing._id = '12345';
		
		this.$scope.deleteUser(this.$scope.users[0]);
		this.$httpBackend.expectDELETE('/users/12345').respond(200);
		spyOn(this.growl, 'addSuccessMessage');
		this.$httpBackend.flush();
		expect(this.growl.addSuccessMessage).toHaveBeenCalled();
		expect(this.$scope.users.length).toEqual(0);
	})


});